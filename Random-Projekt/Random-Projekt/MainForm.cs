﻿/*
 * Created by SharpDevelop.
 * User: Dunkstormen
 * Date: 10-05-2017
 * Time: 13:22
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Random_Projekt
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			listBox1.Items.Add(textBox1.Text);
		}
		
		void Button5Click(object sender, EventArgs e)
		{
			listBox1.Items.Add(listBox2.Items[listBox2.SelectedIndex]);
			listBox2.Items.RemoveAt(listBox2.SelectedIndex);
		}
		
		void Button6Click(object sender, EventArgs e)
		{
			listBox1.Items.Clear();
		}
		
		void Button7Click(object sender, EventArgs e)
		{
			// Test
			listBox2.Items.Clear();
		}
		
		void Button2Click(object sender, EventArgs e)
		{
			listBox2.Items.Add(listBox1.Items[listBox1.SelectedIndex]);
			listBox1.Items.RemoveAt(listBox1.SelectedIndex);
		}
	}
}
